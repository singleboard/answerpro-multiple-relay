
// TODO осталось сравнить текущее время 

#include <PubSubClient.h>
#include <DHT_U.h>
#include <DHT.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include <ArduinoJson.h>

#include <NTPClient.h>
#include <DateTime.h>
#include <ESPDateTime.h>


#include <LinkedList.h>
#include "MyTime.h"

LinkedList<MyTimeRangeClass*> *myLinkedList;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

#define MY_MQTT_MAX_MESSAGE_SIZE 1024
DynamicJsonDocument doc(MY_MQTT_MAX_MESSAGE_SIZE);

MyTimeClass* currentTime;
// work with data time_t to string
// https://github.com/arduino-libraries/NTPClient/issues/36

 
// Uncomment one of the lines bellow for whatever DHT sensor type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
 
// Change the credentials below, so your ESP8266 connects to your router
const char* ssid = "Joynet";
const char* password = "9032562706";
 
// Change the variable to your Raspberry Pi IP address, so it connects to your MQTT broker
const char* mqtt_server = "raspberrypi4";

const char* mqtt_client_label = "AnswerPROESP8266Client";
 
// Initializes the espClient. You should change the espClient name if you have multiple ESPs running in your home automation system
WiFiClient espClient;
PubSubClient client(espClient);
 
//const int DHTPin = 5;
// GPIO 2 = D4
const int DHTPin = 2;

// GPIO 13 = D7
const int DHTPin_in = 13;

 
// Lamp - LED - GPIO 4 = D2 on ESP-12E NodeMCU board
//const int lamp = 4;
 
// Initialize DHT sensor.
DHT dht(DHTPin, DHTTYPE);

DHT dht_in(DHTPin_in, DHTTYPE);
 
// Timers auxiliar variables
long now = millis();
long lastMeasure = 0;

// VARIABLES

// lamp_mode: 0 - manual; 1 - depends on timer
const int lamp_mode = 0;
// GPIO5 = D1
const int lamp_pin = 5;

// GPIO4 = D2
const int pump_pin = 4;

// GPIO2 = D4
const int lamp_temp_pin = 2;
 
// GPIO 14 = D5
const int fan_pin = 14;

// GPIO 12 = D6
const int cond_pin = 12;

uint8_t current_hours = 0, current_mins = 0, current_secs = 0;

uint8_t getHours(time_t rawtime) {
  struct tm * ti;
  ti = localtime (&rawtime);
  uint8_t hours = ti->tm_hour;
  return hours;
}
uint8_t getMins(time_t rawtime) {
  struct tm * ti;
  ti = localtime (&rawtime);
  uint8_t mins = ti->tm_min;
  return mins;
}
uint8_t getSecs(time_t rawtime) {
  struct tm * ti;
  ti = localtime (&rawtime);
  uint8_t secs = ti->tm_sec;
  return secs;
}


// Don't change the function below. This functions connects your ESP8266 to your router
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  
  Serial.println("");
  Serial.print("WiFi connected - ESP IP address: ");
  Serial.println(WiFi.localIP());
}

void changePin(int pin, String messageTemp) {
  String strPin = String(pin);
  Serial.print("Changing PIN " + strPin + " to ");
  if(messageTemp == "on"){
        digitalWrite(pin, HIGH);
        Serial.print("On");
      }
      else if(messageTemp == "off"){
        digitalWrite(pin, LOW);
        Serial.print("Off");
      }
}

// This functions is executed when some device publishes a message to a topic that your ESP8266 is subscribed to
// Change the function below to add logic to your program, so when a device publishes a message to a topic that 
// your ESP8266 is subscribed you can actually do something
void callback(String topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
   
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();
 
  // Feel free to add more if statements to control more GPIOs with MQTT
 
  // If a message is received on the topic room/lamp, you check if the message is either on or off. Turns the lamp GPIO according to the message
  if(topic=="answerpro/lamp"){
    changePin(lamp_pin, messageTemp);

//    timeClient.update();
//    timeClient.forceUpdate();
//    time_t rawtime = timeClient.getEpochTime();
//    uint8_t hours = getHours(rawtime);
//    Serial.println(timeClient.getFormattedTime());
//    Serial.println(hours);
//    Serial.println(getMins(rawtime));
//    Serial.println(getSecs(rawtime));

  }
  if(topic=="answerpro/pump"){
    changePin(pump_pin, messageTemp);
  }
  if(topic=="answerpro/fan"){
    changePin(fan_pin, messageTemp);
  }
  if(topic=="answerpro/cond"){
    changePin(cond_pin, messageTemp);
  }
  if(topic=="answerpro/time"){
    Serial.println(messageTemp);
    // Deserialize the JSON document
    DeserializationError error = deserializeJson(doc, messageTemp);
  
    // Test if parsing succeeds.
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
    }     
    JsonObject obj = doc.as<JsonObject>();

    current_hours = obj["hours"];
    current_mins = obj["mins"];
    current_secs = obj["secs"];
    currentTime->update(current_hours, current_mins, current_secs);
    currentTime->printSerial();
    Serial.println();
  }
  if(topic=="answerpro/lamp_scheduler") {
    Serial.println(messageTemp);

    // Deserialize the JSON document
    DeserializationError error = deserializeJson(doc, messageTemp);
  
    // Test if parsing succeeds.
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
    }    
    
    JsonObject obj = doc.as<JsonObject>();

    myLinkedList->clear();
    Serial.println("111");
    JsonArray timers = obj["timers"];
    for(JsonObject timer : timers) {
      uint8_t starthour = timer["starthour"];
      uint8_t endhour = timer["endhour"];
      uint8_t startmin = timer["startmin"];
      uint8_t endmin = timer["endmin"];
      MyTimeRangeClass* timeRange = new MyTimeRangeClass(starthour, startmin, 0, endhour, endmin, 0);
      timeRange->printSerial();
      Serial.println();
      myLinkedList->add(timeRange);
    }
    Serial.println("222");
    for(int i = 0;i < myLinkedList->size();i++) {
       MyTimeRangeClass* timeRange = myLinkedList->get(i); 
       timeRange->printSerial();
       Serial.println();
    }
    Serial.println("333");
    
  }
  Serial.println();
}
 
// This functions reconnects your ESP8266 to your MQTT broker
// Change the function below if you want to subscribe to more topics with your ESP8266 
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    /*
     YOU MIGHT NEED TO CHANGE THIS LINE, IF YOU'RE HAVING PROBLEMS WITH MQTT MULTIPLE CONNECTIONS
     To change the ESP device ID, you will have to give a new name to the ESP8266.
     Here's how it looks:
       if (client.connect(mqtt_client_label)) {
     You can do it like this:
       if (client.connect("ESP1_Office")) {
     Then, for the other ESP:
       if (client.connect("ESP2_Garage")) {
      That should solve your MQTT multiple connections problem
    */
    if (client.connect(mqtt_client_label)) {
      Serial.println("connected");  
      // Subscribe or resubscribe to a topic
      // You can subscribe to more topics (to control more LEDs in this example)
      client.subscribe("answerpro/lamp");
      client.subscribe("answerpro/pump");

      client.subscribe("answerpro/fan");
      client.subscribe("answerpro/cond");
      
      client.subscribe("answerpro/compressor");
      client.subscribe("answerpro/free_relay");

      client.subscribe("answerpro/lamp_scheduler");

      client.subscribe("answerpro/time");
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
 
// The setup function sets your ESP GPIOs to Outputs, starts the serial communication at a baud rate of 115200
// Sets your mqtt broker and sets the callback function
// The callback function is what receives messages and actually controls the LEDs
void setup() {
  pinMode(lamp_pin, OUTPUT);
  pinMode(pump_pin, OUTPUT);
  pinMode(fan_pin, OUTPUT);
  pinMode(cond_pin, OUTPUT);

  myLinkedList = new LinkedList<MyTimeRangeClass*>();
  currentTime = new MyTimeClass(0, 0, 0);
   
  dht.begin();
  dht_in.begin();
   
  Serial.begin(115200);
  setup_wifi();

//  timeClient.begin();
//  timeClient.update();
//  Serial.println(timeClient.getFormattedTime());

  client.setBufferSize(MY_MQTT_MAX_MESSAGE_SIZE);
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
 
}

void publicDHT(DHT dht, String topic_suffix) {
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    float h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    float t = dht.readTemperature();
    // Read temperature as Fahrenheit (isFahrenheit = true)
    float f = dht.readTemperature(true);
 
    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t) || isnan(f)) {
      Serial.println("Failed to read from DHT sensor!");
      return;
    }
 
    // Computes temperature values in Celsius
    float hic = dht.computeHeatIndex(t, h, false);
    static char temperatureTemp[7];
    dtostrf(hic, 6, 2, temperatureTemp);
     
    static char humidityTemp[7];
    dtostrf(h, 6, 2, humidityTemp);
 
    // Publishes Temperature and Humidity values
    String topic = "answerpro/" + topic_suffix;
//    (char*) topic.c_str();
    client.publish((char*) topic.c_str(), temperatureTemp);
//    client.publish("answerpro/h_lamp", humidityTemp);
//    Serial.print("Temparature: ");
    Serial.println(temperatureTemp);
  
}
 
// For this project, you don't need to change anything in the loop function. Basically it ensures that you ESP is connected to your broker
void loop() {
  Serial.print("Lamp status: ");
  Serial.println(digitalRead (lamp_pin));
 
  if (!client.connected()) {
    reconnect();
  }
  if(!client.loop())
    client.connect(mqtt_client_label);
 
  now = millis();
  // Publishes new temperature and humidity every 30 seconds
  if (now - lastMeasure > 10000) {
    lastMeasure = now;

    publicDHT(dht, "t_lamp");
    publicDHT(dht_in, "t_in");
     
//    Serial.println("Kuku");
    bool lamp_flag = false;
    for(int i = 0;i < myLinkedList->size();i++) {
      MyTimeRangeClass* timeRange = myLinkedList->get(i); 
      timeRange->printSerial();
      Serial.println();

      bool flag = timeRange->contain(currentTime);
      if(flag) {
        lamp_flag = true;
      }
    }
    if(lamp_flag) {
      changePin(lamp_pin, "on");
      Serial.println("Lamp must be On");
    } else {
      changePin(lamp_pin, "off");
      Serial.println("Lamp must be Off");
    }

  }
} 
