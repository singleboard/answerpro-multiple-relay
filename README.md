# 

## Run in docker
```bash
# run mosquitto
docker run -it -d --rm  -p 1883:1883 -p 9001:9001 --name mosquitto eclipse-mosquitto

# run node-red
docker run -it -d --rm -p 1880:1880 -v $PWD/node_red_data:/data --name nodered nodered/node-red
```

Connect docker between each other

## 