class MyTimeClass {

  public: 
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
   
    MyTimeClass(uint8_t hours = 0, uint8_t minutes = 0, uint8_t seconds = 0) {
      this->hours = hours;
      this->minutes = minutes;
      this->seconds = seconds;
    }

    void update(uint8_t hours = 0, uint8_t minutes = 0, uint8_t seconds = 0) {
      this->hours = hours;
      this->minutes = minutes;
      this->seconds = seconds;
    }

    int toSeconds() {
      return 3600*this->hours + 60*this->minutes + this->seconds;
    }

    void printSerial() {
      Serial.print("Time:");
      Serial.print(this->hours);
      Serial.print(":");
      Serial.print(this->minutes);
      Serial.print(":");
      Serial.print(this->seconds);
    }
    
    static bool between(MyTimeClass* startTime, MyTimeClass* stopTime, MyTimeClass* t) {
      Serial.println("between");
      return true;
    }
};


class MyTimeRangeClass {

  private:
    MyTimeClass* startTime;
    MyTimeClass* stopTime;
    
  public: 
    MyTimeRangeClass(uint8_t h1 = 0, uint8_t m1 = 0, uint8_t s1 = 0,
                uint8_t h2 = 0, uint8_t m2 = 0, uint8_t s2 = 0) {
      this->startTime = new MyTimeClass(h1, m1, s1);
      this->stopTime = new MyTimeClass(h2, m2, s2);
    }

    ~MyTimeRangeClass() {
      delete startTime;
      delete stopTime;
    }

    bool contain(MyTimeClass* t) {
      int startSec = this->startTime->toSeconds();
      int stopSec = this->stopTime->toSeconds();
      int iSec = t->toSeconds();
      if(iSec >= startSec && iSec <= stopSec)
        return true;
      else
        return false;
    }

    void printSerial() {
      Serial.print("StartTime: ");
      startTime->printSerial();
      Serial.print(" ");
      Serial.print("StopTime: ");
      stopTime->printSerial();
    }

};
